# @version ^0.2.0

# @dev Crowdfundable ERC-20 token with limited supply and bonding curve
# @author Norlin Games (https://twitter.com/NorlinGames)
# https://gitlab.com/KnightmareLands/slime-erc20

# @dev Implementation of ERC-20 token standard.
# @author Takayuki Jimba (@yudetamago)
# https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md

from vyper.interfaces import ERC20

implements: ERC20

event Transfer:
    sender: indexed(address)
    receiver: indexed(address)
    value: uint256

event Approval:
    owner: indexed(address)
    spender: indexed(address)
    value: uint256

name: public(String[64])
symbol: public(String[32])
decimals: public(uint256)

balanceOf: public(HashMap[address, uint256])
allowance: public(HashMap[address, HashMap[address, uint256]])
totalSupply: public(uint256)

CURVE_PRECISION: constant(uint256) = 6
CURVE_RATIO: constant(uint256) = 166

basePrice: public(uint256)
payments: public(HashMap[address, uint256])

endTime: public(uint256)

norlin: address


@external
def __init__(_base: uint256, _duration: uint256):

    self.name = "Slime Coin"
    self.symbol = "SLIME"
    self.decimals = 0

    # $1 in wei at the time of the contract deployment
    # e.g. $1 = 0.00053 ETH
    # self.basePrice = 530000000000000
    self.basePrice = _base

    # no pre-minted tokens
    # The supply will not be changed on the token claims
    self.totalSupply = 0

    # duration of the crowdfunding campaing
    # till that time, the funds can't be withdrawn by anyone
    # assuming it to be now + 7777777 (~90 days since launch)
    self.endTime = block.timestamp + _duration
    # self.endTime = block.timestamp + 7777777

    # set norlin as the owner
    self.norlin = msg.sender


@external
def transfer(_to : address, _value : uint256) -> bool:
    """
    @dev Transfer token for a specified address
    @param _to The address to transfer to.
    @param _value The amount to be transferred.
    """

    assert _to != ZERO_ADDRESS

    # NOTE: vyper does not allow underflows
    #       so the following subtraction would revert on insufficient balance
    self.balanceOf[msg.sender] -= _value
    self.balanceOf[_to] += _value

    log Transfer(msg.sender, _to, _value)
    return True


@external
def transferFrom(_from : address, _to : address, _value : uint256) -> bool:
    """
     @dev Transfer tokens from one address to another.
     @param _from address The address which you want to send tokens from
     @param _to address The address which you want to transfer to
     @param _value uint256 the amount of tokens to be transferred
    """

    assert _to != ZERO_ADDRESS

    # NOTE: vyper does not allow underflows
    #      so the following subtraction would revert on insufficient allowance
    self.allowance[_from][msg.sender] -= _value

    # NOTE: vyper does not allow underflows
    #       so the following subtraction would revert on insufficient balance
    self.balanceOf[_from] -= _value
    self.balanceOf[_to] += _value

    log Transfer(_from, _to, _value)
    return True


@external
def approve(_spender : address, _value : uint256) -> bool:
    """
    @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
         Beware that changing an allowance with this method brings the risk that someone may use both the old
         and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
         race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
         https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    @param _spender The address which will spend the funds.
    @param _value The amount of tokens to be spent.
    """

    self.allowance[msg.sender][_spender] = _value
    log Approval(msg.sender, _spender, _value)
    return True


@internal
def _mint(_to: address, _value: uint256):
    """
    @dev Mint an amount of the token and assigns it to an account.
         This encapsulates the modification of balances such that the
         proper events are emitted.
         Only contract itself can mint the tokens
    @param _to The account that will receive the created tokens.
    @param _value The amount that will be created.
    """

    assert _to != ZERO_ADDRESS

    self.totalSupply += _value
    self.balanceOf[_to] += _value

    log Transfer(ZERO_ADDRESS, _to, _value)


@internal
@pure
def _getPrice(_base: uint256, _supply: uint256) -> uint256:
    """
    @dev Returns the price for next token when currently there are "supply" tokens exist
    @param _supply Current amount of minted tokens
    """

    if _supply == 0:
        return _base

    k: uint256 = _supply
    n: uint256 = _supply - 1

    s: uint256 = 0
    N: uint256 = 1
    B: uint256 = 1

    for i in range(CURVE_PRECISION):
        s += k * N / B / (CURVE_RATIO**i)
        if n > i:
            N = N * (n-i)
        B = B * (i+1)

    return _base * s


@external
@view
def getPrice() -> uint256:
    """
    @dev Returns the price for next token when currently there are "supply" tokens exist
    """
    return self._getPrice(self.basePrice, self.totalSupply)


@external
@view
def getPriceNth(_nth: uint256) -> uint256:
    """
    @dev Returns the price for Nth token
    """
    return self._getPrice(self.basePrice, _nth)


@external
@payable
def purchase():
    # Can't mint tokens after the end time
    assert block.timestamp < self.endTime

    self.payments[msg.sender] += msg.value

    price: uint256 = self._getPrice(self.basePrice, self.totalSupply)
    amount: uint256 = 1

    # TODO: allow to purchase multiple tokens at once
    # Maybe increment the price in a loop increasing the supply value
    # Or implement canPurchase method which returns the amount of how much possible to purchase

    if self.payments[msg.sender] < price:
        return

    self.payments[msg.sender] -= (price * amount)
    self._mint(msg.sender, amount)

