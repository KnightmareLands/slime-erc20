import pytest
import brownie

INITIAL_SUPPLY = 0
BASE_PRICE = 1
DURATION = 7777777
MAX_SUPPLY = 500
TOTAL_PRICE_MIN = 1000000
TOTAL_PRICE_MAX = 1100000


@pytest.fixture
def slime_contract(SlimeCoin, accounts):
    # deploy the contract with the initial value as a constructor argument
    yield SlimeCoin.deploy(BASE_PRICE, DURATION, {'from': accounts[0]})


def test_initial_state(slime_contract):
    # Check if the constructor of the contract is set up properly
    assert slime_contract.totalSupply() == INITIAL_SUPPLY
    assert slime_contract.getPrice() == BASE_PRICE


def test_price(slime_contract):
    # Test the total price for first MAX_SUPPLY tokens
    total = 0
    for i in range(MAX_SUPPLY):
        total += slime_contract.getPriceNth(i)

    # TODO: In addition to the total, also check for the curve
    # Maybe just check for multiple values (10th, 50th, 100th, 200th, 300th, 400th, 500th)

    # assuming to collect ~1 mil from the first 500 tokens
    assert total > TOTAL_PRICE_MIN and total < TOTAL_PRICE_MAX

